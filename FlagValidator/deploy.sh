docker build . -t tomasfreund/test:flag-validator
docker push tomasfreund/test:flag-validator

kubectl create namespace flag-validator
kubectl create secret --namespace flag-validator generic flag --from-literal=FLAG=${FLAG}
kubectl apply --namespace flag-validator -f deploy/deployment.yaml
kubectl apply --namespace flag-validator -f deploy/service.yaml
kubectl apply --namespace flag-validator -f deploy/ingress.yaml