# Ingress CTF

## Overview

This app exists to validate the flag you are supposed to capture, it exists inside the `FLAG` environment variable inside my app.

I don't even know why I created this, you can't really get the flag anyway because my app is totally secure, right?

## Challenge

This app is running at https://flag-validator.freund.dev/ (source code and kubernetes yaml files match the ones in the repo)

Capture the flag and then `POST` it in the request body to the `/validate` endpoint to validate.

Please don't attempt any DoS attack (it's running on a NUC under my router and my internet is not the best) or any sophisticated attacks - `curl` or any other http client is all you need.

If you need a hint, message me in chat or send me an e-mail.