using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FlagValidator
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async ctx =>
                {
                    ctx.Response.StatusCode = 200;
                    await ctx.Response.WriteAsync(
                        "Hi, check the repository for more info https://gitlab.com/tomas-freund-ctf/flag-validator/-/tree/master/FlagValidator\nOnce you have the flag, POST it to /validate in the request body to see if it's correct.");
                });
                endpoints.MapPost("/validate", async ctx =>
                {
                    var flag = Environment.GetEnvironmentVariable("FLAG");
                    using var sr = new StreamReader(ctx.Request.Body);
                    var submittedFlag = await sr.ReadToEndAsync();
                    if (flag == submittedFlag)
                    {
                        ctx.Response.StatusCode = 200;
                        await ctx.Response.WriteAsync("Congratulations, you captured the flag");
                    }
                    else
                    {
                        ctx.Response.StatusCode = 400;
                        await ctx.Response.WriteAsync("That is not the correct flag value");
                    }
                });
                // this is a debugging endpoint only exposed on internal port (not accessible from outside)
                endpoints.MapGet("/flag", async ctx =>
                {
                    ctx.Response.StatusCode = 200;
                    await ctx.Response.WriteAsync($"Flag: '{Environment.GetEnvironmentVariable("FLAG")}'");
                }).RequireHost("*:1234");
            });
        }
    }
}
